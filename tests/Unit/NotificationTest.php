<?php

namespace Tests\Unit;

use App\Notification;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class NotificationTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $notification1 = factory(Notification::class)->create();
        $notification2 = factory(Notification::class)->create();
        $notifications = Notification::get()->makeVisible('user_id')->toArray();

        $this->assertCount(2, $notifications);
        $this->assertEquals([
            [
                'id' => $notification1->id,
                'is_dismissed' => $notification1->is_dismissed,
                'user_id' => $notification1->user_id,
                'transfer_id' => $notification1->transfer_id,
                'message' => $notification1->message,
                'created_at' => $notification1->created_at->toDateTimeString(),
                'updated_at' => $notification1->updated_at->toDateTimeString(),
                'deleted_at' => $notification1->deleted_at->toDateTimeString(),
            ],
            [
                'id' => $notification2->id,
                'is_dismissed' => $notification2->is_dismissed,
                'user_id' => $notification2->user_id,
                'transfer_id' => $notification2->transfer_id,
                'message' => $notification2->message,
                'created_at' => $notification2->created_at->toDateTimeString(),
                'updated_at' => $notification2->updated_at->toDateTimeString(),
                'deleted_at' => $notification2->deleted_at->toDateTimeString(),
            ],
        ], $notifications);
    }
}
