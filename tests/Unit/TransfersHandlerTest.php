<?php

namespace Tests\Unit;

use App\Libraries\ExchangeAccrualRateCalculator;
use App\Libraries\TransfersHandler;
use App\Libraries\UserBalanceSummaryCalculator;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TransfersHandlerTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $accumulatedAmountForUser = 0;

    public function setUp()
    {
        parent::setUp();

        $now = Carbon::now();
        $userCreatedAt = $now->copy()->subMinutes(120);
        $this->user = factory(User::class)->create(['created_at' => $userCreatedAt]);

        $this->accumulatedAmountForUser = ExchangeAccrualRateCalculator::i()->calculateAccrual(
            $userCreatedAt,
            $now,
            UserBalanceSummaryCalculator::ACCRUAL_RATE_PERIOD_MINUTES,
            UserBalanceSummaryCalculator::ACCRUAL_RATE_PER_RATE_PERIOD_MINUTES
        );
    }

    public function testProcessRecipients()
    {
        $recipient1 = factory(User::class)->create();
        $recipient2 = factory(User::class)->create();
        $results = TransfersHandler::i()->processRecipients($this->user, [
            ['email' => $recipient1->email, 'amount' => $this->accumulatedAmountForUser / 2],
            ['email' => $recipient2->email, 'amount' => $this->accumulatedAmountForUser / 2],
        ]);
        $this->assertCount(2, $results);
    }

    public function testProcessRecipientsBalanceNotAvailable()
    {
        $this->expectException(\Exception::class);
        $recipient1 = factory(User::class)->create();
        $recipient2 = factory(User::class)->create();
        $results = TransfersHandler::i()->processRecipients($this->user, [
            ['email' => $recipient1->email, 'amount' => $this->accumulatedAmountForUser],
            ['email' => $recipient2->email, 'amount' => $this->accumulatedAmountForUser],
        ]);
    }

    public function testRemainderBalance()
    {
        $remainder = 0.01;
        $recipient1 = factory(User::class)->create();
        $recipient2 = factory(User::class)->create();
        $results = TransfersHandler::i()->processRecipients($this->user, [
            ['email' => $recipient1->email, 'amount' => $this->accumulatedAmountForUser / 2],
            ['email' => $recipient2->email, 'amount' => ($this->accumulatedAmountForUser / 2) - $remainder],
        ]);
        $userBalanceSummary = UserBalanceSummaryCalculator::i()->getSummary($this->user);
        $this->assertEquals($userBalanceSummary['balance'], $remainder);
    }

    public function testNotificationsWereCreated()
    {
        $recipient1 = factory(User::class)->create();
        $recipient2 = factory(User::class)->create();
        $results = TransfersHandler::i()->processRecipients($this->user, [
            ['email' => $recipient1->email, 'amount' => $this->accumulatedAmountForUser / 2],
            ['email' => $recipient2->email, 'amount' => ($this->accumulatedAmountForUser / 2)],
        ]);
        $this->assertDatabaseHas('notifications', ['transfer_id' => $results[0]->id]);
        $this->assertDatabaseHas('notifications', ['transfer_id' => $results[1]->id]);
    }
}
