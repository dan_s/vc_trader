<?php

namespace Tests\Unit;

use App\Transfer;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TransferTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $transfer1 = factory(Transfer::class)->create();
        $transfer2 = factory(Transfer::class)->create();
        $transfers = Transfer::get()->makeVisible('user_id')->toArray();

        $this->assertCount(2, $transfers);
        $this->assertEquals([
            [
                'user_id' => $transfer1->user_id,
                'recipient_user_id' => $transfer1->recipient_user_id,
                'amount' => $transfer1->amount,
                'created_at' => $transfer1->created_at->toDateTimeString(),
                'updated_at' => $transfer1->updated_at->toDateTimeString(),
            ],
            [
                'user_id' => $transfer2->user_id,
                'recipient_user_id' => $transfer2->recipient_user_id,
                'amount' => $transfer2->amount,
                'created_at' => $transfer2->created_at->toDateTimeString(),
                'updated_at' => $transfer2->updated_at->toDateTimeString(),
            ],
        ], $transfers);
    }
}
