<?php

namespace Tests\Unit;

use App\Libraries\ExchangeAccrualRateCalculator;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class ExchangeAccrualRateCalculatorTest extends TestCase
{
    public function testGetAccrualSinceDate()
    {
        $now = Carbon::now('America/Vancouver');
        $calculator = new ExchangeAccrualRateCalculator();
        $total = $calculator->calculateAccrual($now->copy()->subMinutes(30), $now, 30, 0.5);
        $this->assertEquals(0.5, $total);
    }

    public function testAccrualEqualsZeroForReversedDates()
    {
        $now = Carbon::now('America/Vancouver');
        $calculator = new ExchangeAccrualRateCalculator();
        $total = $calculator->calculateAccrual($now->copy()->addMinutes(30), $now, 30, 0.5);
        $this->assertEquals(0, $total);
    }

    public function testAccrualForLongDuration()
    {
        $calculator = new ExchangeAccrualRateCalculator();
        $total = $calculator->calculateAccrual(
            Carbon::parse('January 1 2013', 'America/Vancouver'),
            Carbon::parse('June 1 2018', 'America/Vancouver'),
            1,
            1
        );
        $this->assertEquals(2846880, $total);
    }

    public function testAccrualForDifferentTimeZones()
    {
        $calculator = new ExchangeAccrualRateCalculator();
        $total = $calculator->calculateAccrual(
            Carbon::parse('January 1 2018', 'UTC'),
            Carbon::parse('January 1 2018', 'America/Vancouver'),
            60,
            1
        );
        $this->assertEquals(8, $total);
    }

    public function testAccrualRateNegatives()
    {
        $calculator = new ExchangeAccrualRateCalculator();
        $total = $calculator->calculateAccrual(
            Carbon::parse('January 1 2018', 'UTC'),
            Carbon::parse('January 1 2018', 'America/Vancouver'),
            -10,
            1
        );
        $this->assertEquals(0, $total);

        $total = $calculator->calculateAccrual(
            Carbon::parse('January 1 2018', 'UTC'),
            Carbon::parse('January 1 2018', 'America/Vancouver'),
            10,
            -0.5
        );
        $this->assertEquals(0, $total);
    }
}
