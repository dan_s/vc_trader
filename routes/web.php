<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/balance-summary', 'UserBalanceSummaryController@show')->middleware('auth');
Route::get('/user-lookup', 'UserLookupController@show')->middleware('auth');

Route::get('/notifications', 'NotificationsController@index')->middleware('auth');
Route::post('/notifications', 'NotificationsController@dismiss')->middleware('auth');


Route::resource('transfers', 'TransfersController', [
    'names' => [
        'index' => 'transfers.index',
        'create' => 'transfers.create',
    ],
])->middleware('auth');
