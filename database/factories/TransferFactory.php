<?php

use Faker\Generator as Faker;

$factory->define(App\Transfer::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'recipient_user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'amount' => $faker->randomFloat(2),
    ];
});
