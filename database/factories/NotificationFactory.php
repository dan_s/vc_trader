<?php

use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Notification::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'transfer_id' => function () {
            return factory(App\Transfer::class)->create()->id;
        },
        'message' => $faker->sentence,
        'is_dismissed' => false,
        'deleted_at' => Carbon::now(),
    ];
});
