@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="jumbotron">
                    <h1>Welcome</h1>
                    <h3>To the most boring currency transfer app you will ever visit!</h3>
                    <p>Get started by clicking one of the buttons below</p>
                    <p>
                        <a class="btn btn-lg btn-primary" href="{{ route('transfers.create') }}">Send to People</a>
                        <a class="btn btn-lg btn-primary" href="{{ route('transfers.index') }}">Your Transfers</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
