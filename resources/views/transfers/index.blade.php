@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Transfers</div>

                <div class="panel-body">
                    <transfer-listing></transfer-listing>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
