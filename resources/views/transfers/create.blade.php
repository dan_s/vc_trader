@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">New Transfer</div>

                <div class="panel-body">
                    <new-transfer-form :current-user-balance="{!! $currentUserBalance !!}"></new-transfer-form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
