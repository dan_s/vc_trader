<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function transfer()
    {
        return $this->hasOne(Transfer::class, 'id', 'transfer_id');
    }
}
