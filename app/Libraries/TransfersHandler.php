<?php
namespace App\Libraries;

use App\Transfer;
use App\User;

class TransfersHandler
{
    public static function i()
    {
        return new TransfersHandler();
    }

    /**
     * Processes a list of recipients to transfer currency to and sends them notifications
     *
     * @param  User  $user
     * @param  array $recipients
     * @return array
     * @throws \Exception
     */
    public function processRecipients(User $user, array $recipients): array
    {
        if (!$this->canTransactionsBegin($user, $recipients)) {
            throw new \Exception('Balance not available');
        }

        $transfers = [];
        foreach ($recipients as $recipient) {
            $transfers[] = $this->processRecipient(
                $user,
                $recipient['email'],
                $recipient['amount']
            );
        }

        return $transfers;
    }

    /**
     * Checks the user's available balance and returns false if the total of all of the transactions
     * exceeds the user's available balance
     *
     * @param  User  $user
     * @param  array $recipients
     * @return bool
     */
    private function canTransactionsBegin(User $user, array $recipients): bool
    {
        $userBalanceSummary = UserBalanceSummaryCalculator::i()->getSummary($user);
        $transactionTotal = $this->calculateTransactionTotal($recipients);

        return $userBalanceSummary['balance'] >= $transactionTotal;
    }

    /**
     * Calculates the sum of all the recipient's amounts
     *
     * @param  array $recipients
     * @return float
     */
    private function calculateTransactionTotal(array $recipients): float
    {
        $total = 0;
        foreach ($recipients as $recipient) {
            $total += $recipient['amount'];
        }

        return $total;
    }

    /**
     * Creates a transfer and notification records for a transaction
     *
     * @param  User           $user
     * @param  $recipientEmail
     * @param  $amount
     * @return Transfer
     */
    private function processRecipient(User $user, $recipientEmail, $amount): Transfer
    {
        $receiver = User::where('email', $recipientEmail)->firstOrFail();
        $transfer = $this->createTransferRecord($user, $receiver, $amount);
        //todo make this an event someday
        $notification = NotificationHandler::i()->createForTransfer($receiver, $transfer);

        return $transfer;
    }

    /**
     * Creates a new transfer record
     *
     * @param  User   $sender
     * @param  User   $receiver
     * @param  $amount
     * @return Transfer
     */
    private function createTransferRecord(User $sender, User $receiver, $amount): Transfer
    {
        $transfer = new Transfer();
        $transfer->user_id = $sender->id;
        $transfer->recipient_user_id = $receiver->id;
        $transfer->amount = $amount;
        $transfer->save();

        return $transfer;
    }
}
