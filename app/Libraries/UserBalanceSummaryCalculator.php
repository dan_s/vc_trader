<?php
namespace App\Libraries;

use App\Transfer;
use App\User;
use Carbon\Carbon;

class UserBalanceSummaryCalculator
{
    const ACCRUAL_RATE_PERIOD_MINUTES = 30;
    const ACCRUAL_RATE_PER_RATE_PERIOD_MINUTES = 0.25;

    /**
     * @return UserBalanceSummaryCalculator
     */
    public static function i()
    {
        return new UserBalanceSummaryCalculator();
    }

    /**
     * Gets a summary object of the user's accrued amount, their debits and credits,
     * and the overall balance for the user
     *
     * @param  User $user
     * @return array
     */
    public function getSummary(User $user)
    {
        $accrued = ExchangeAccrualRateCalculator::i()->calculateAccrual(
            $user->created_at,
            Carbon::now(),
            self::ACCRUAL_RATE_PERIOD_MINUTES,
            self::ACCRUAL_RATE_PER_RATE_PERIOD_MINUTES
        );
        $debits = (float)Transfer::where('recipient_user_id', $user->id)->sum('amount');
        $credits = (float)$user->sentTransfers()->sum('amount');

        return [
            'balance' => $accrued + $debits - $credits,
            'accrued' => $accrued,
            'credits' => $credits,
            'debits' => $debits,
        ];
    }
}
