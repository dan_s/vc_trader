<?php

namespace App\Libraries;

use Carbon\Carbon;

class ExchangeAccrualRateCalculator
{
    public static function i()
    {
        return new ExchangeAccrualRateCalculator();
    }

    /**
     * Calculates an accrued amount until based on the start/end date, an accrual period in minutes, and an accrual rate
     *
     * @param  Carbon $startDate
     * @param  Carbon $endDate
     * @param  float  $accrualPeriodMinutes
     * @param  float  $accrualRate
     * @return float
     */
    public function calculateAccrual(Carbon $startDate, Carbon $endDate, float $accrualPeriodMinutes, float $accrualRate): float
    {
        if ($startDate->gt($endDate) || $accrualPeriodMinutes <= 0 || $accrualRate <= 0) {
            return 0;
        }

        $accruedMinutesSinceLastDate = $startDate->diffInMinutes($endDate);

        return $accruedMinutesSinceLastDate / $accrualPeriodMinutes * $accrualRate;
    }
}
