<?php
namespace App\Libraries;

use App\Notification;
use App\Transfer;
use App\User;

class NotificationHandler
{
    public static function i()
    {
        return new NotificationHandler();
    }

    /**
     * Adds a notification entry for a transfer for a recipient user
     *
     * @param  User     $recipient
     * @param  Transfer $transfer
     * @return Notification
     */
    public function createForTransfer(User $recipient, Transfer $transfer)
    {
        $notification = new Notification();
        $notification->user_id = $recipient->id;
        $notification->transfer_id = $transfer->id;
        $notification->is_dismissed = false;
        $notification->message = null;

        $notification->save();

        return $notification;
    }

    /**
     * Creates a notification record with a simple message for a recipient user
     *
     * @param  User    $recipient
     * @param  $message
     * @return Notification
     */
    public function createGeneralMessage(User $recipient, $message)
    {
        $notification = new Notification();
        $notification->user_id = $recipient->id;
        $notification->transfer_id = null;
        $notification->is_dismissed = false;
        $notification->message = $message;
        $notification->save();

        return $notification;
    }
}
