<?php

namespace App\Http\Controllers;

use App\Libraries\TransfersHandler;
use App\Libraries\UserBalanceSummaryCalculator;
use App\Transfer;
use Illuminate\Http\Request;

class TransfersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return response(
                [
                'balanceSummary' => UserBalanceSummaryCalculator::i()->getSummary($request->user()),
                'sent' => Transfer::where('user_id', $request->user()->id)
                                  ->orderBy('created_at', 'DESC')
                                  ->with('recipient')
                                  ->get(),
                'received' => Transfer::where('recipient_user_id', $request->user()->id)
                                      ->orderBy('created_at', 'DESC')
                                      ->with('user')
                                      ->get(),
                ]
            );
        }

        return view('transfers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $userBalanceSummary = UserBalanceSummaryCalculator::i()->getSummary($request->user());

        return view(
            'transfers.create',
            [
            'currentUserBalance' => $userBalanceSummary['balance'],
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate(
            [
            'recipients' => 'array',
            'recipients.*.email' => 'required|email|exists:users,email',
            'recipients.*.amount' => 'required|numeric|min:0.01',
            ]
        );
        try {
            return TransfersHandler::i()->processRecipients($request->user(), $data['recipients']);
        } catch (\Exception $exception) {
            return response(['message' => $exception->getMessage()], 422);
        }
    }
}
