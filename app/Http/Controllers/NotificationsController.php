<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    public function index(Request $request)
    {
        return [
            'notifications' => $request->user()
                ->notifications()
                ->with('transfer.user')
                ->orderBy('is_dismissed', 'ASC')
                ->orderBy('created_at', 'DESC')
                ->limit(20)
                ->get(),
        ];
    }

    public function dismiss(Request $request)
    {
        Notification::where('user_id', $request->user()->id)
                    ->whereIn('id', $request->input('notificationsToDismiss'))
                    ->update(['is_dismissed' => 1]);

        return ['success' => true];
    }
}
