<?php

namespace App\Http\Controllers;

use App\Libraries\UserBalanceSummaryCalculator;
use Illuminate\Http\Request;

class UserBalanceSummaryController extends Controller
{
    public function show(Request $request)
    {
        return UserBalanceSummaryCalculator::i()->getSummary($request->user());
    }
}
