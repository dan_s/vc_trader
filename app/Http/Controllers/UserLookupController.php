<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserLookupController extends Controller
{
    public function show(Request $request)
    {
        return [
            'exists' => User::where('email', $request->input('email'))
                            ->where('email', '!=', $request->user()->email)
                            ->exists(),
        ];
    }
}
